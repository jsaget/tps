type graphe = int list array

let indices (n_essais: int): int list =
    (* Renvoie la liste [0, ..., n_essais] dans laquel chaque élément a 9/10
     * chances de ne pas être présent.
     *)
    let rec aux (n: int) (accu: int list): int list =
        if n = 0 then
            accu
        else if Random.float 1.0 < 0.1 then
            aux (n-1) ((n-1) :: accu)
        else
            aux (n-1) accu
    in
    aux n_essais []


let genere (n: int): graphe =
    (* Génère un graphe orienté à n sommets avec une densité d'arêtes de 1/10.
     *)
    Array.init n (fun _ -> indices n)


let rec aux1 (vl: int list) (p: int list) (e: int array): int list =
    match vl with
    | [] -> p
    | y :: vl ->
        if e.(y) = 0 then
            aux1 vl (y :: p) e
        else aux1 vl p e


let rec aux2 (p : int list) (e: int array) (g: int list array) (c: int): unit =
    match p with
    | [] -> ()
    | v :: p ->
        e.(v) <- c;
        aux2 (aux1 g.(v) p e) e g c


let p (g: int list array) =
    let n = Array.length g in
    let e = Array.make n 0 in
    let c = ref 0 in

    for x = 0 to n - 1 do
        while e.(x) = 0 do
            incr c;
            aux2 (x :: []) e g !c
        done
    done;
    e


let rec filtre (candidats: int list) (recus: int list)
    (critere: int -> bool): int list
=
    (* Ajoute les candidats qui respectent le critère à la liste des reçus *)
    match candidats with
    | [] -> recus
    | c :: candidats when critere c -> filtre candidats (c :: recus) critere
    | _ :: candidats -> filtre candidats recus critere


let rec visite (g: graphe) (rangs: int array) (rang_actuel: int)
    (sommets: int list): unit
=
    (* Visite les sommets demandés et leurs descendants par encore visités, et
     * indique que leur rang est le rang actuel
     *)
    match sommets with
    | [] -> ()  (* visite terminée *)
    | s :: sommets ->
        rangs.(s) <- rang_actuel;
        let critere = fun n -> rangs.(n) = 0 in (* Critère : sommet non visité *)
        let nouveaux_sommets = filtre g.(s) sommets critere in
        visite g rangs rang_actuel nouveaux_sommets


let parcours_en_profondeur (g: graphe): int array =
    (* Effectue des parcours en profondeur du graphe g dans l'ordre des sommets
     * et indique quels sommets ont été vus en même temps lors du parcours
     *)
    let n = Array.length g in
    let rangs = Array.make n 0 in (* 0 veut dire pas encore vu *)
    let rang_actuel = ref 0 in

    for x = 0 to n-1 do
        if rangs.(x) = 0 then begin
            incr rang_actuel;
            visite g rangs !rang_actuel (x :: [])
        end
    done;

    rangs


let () =
    let n = 10 in
    let g = genere n in
    let c = p g in
    let c' = parcours_en_profondeur g in
    for i = 0 to n-1 do
        Printf.printf "%d -> %d, %d\n" i c.(i) c'.(i)
    done
