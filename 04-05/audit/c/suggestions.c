#include <stdio.h>
#include <stdlib.h>

struct arbre {
    struct arbre **enfants;
    int n_enfants;
    int poids;
};


int poids_total(struct arbre *a)
{
    /* Calcul le poids total d'un arbre (somme du poids de la racine et des
     * poids des sous arbres).
     */
    int s = a->poids;

    for (int i = 0; i < a->n_enfants; i++) {
        s += poids_total(a->enfants[i]);
    }
    return s;
}


struct arbre *cree(struct arbre **enfants, int n_enfants, int poids)
{
    /* Crée un arbre à partir d'un tableau d'enfants, du nombre d'enfants et du
     * poids de la racine.
     */
    struct arbre *a = malloc(sizeof(*a));

    a->enfants = enfants;
    a->n_enfants = n_enfants;
    a->poids = poids;

    return a;
}


void supprime(struct arbre *a)
{
    /* Libère l'arbre passé en argument, sauf si celui-ci est utilisé au sein
     * d'un autre arbre.
     */
    if (a != NULL) {
        for(int i = 0; i < a->n_enfants; i++) {
            supprime(a->enfants[i]);
        }

        free(a->enfants);
        free(a);
    }
}


int main(int argc, char **argv)
{
    struct arbre *a0, *a1, *b, **b_enfants;

    a0 = cree(NULL, 0, 1);
    a1 = cree(NULL, 0, 1);
    b_enfants = malloc(sizeof(*b_enfants) * 2);
    b_enfants[0] = a0;
    b_enfants[1] = a1;

    b = cree(b_enfants, 2, 1);

    printf("%d\n", poids_total(b));
    supprime(b);

    return 0;
}
