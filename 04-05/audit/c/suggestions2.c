#include <stdio.h>
#include <stdlib.h>

struct dag {
    struct dag **enfants;
    int n_enfants;
    int poids;
    int refs;   // Compte le nombre de fois où cette structure est référencée
};


int poids_total(struct dag *d)
{
    /* Calcul le poids total d'un arbre (somme du poids de la racine et des
     * poids des sous arbres).
     */
    int s = d->poids;

    for (int i = 0; i < d->n_enfants; i++) {
        s += poids_total(d->enfants[i]);
    }
    return s;
}


struct dag *cree(struct dag **enfants, int n_enfants, int poids)
{
    /* Crée un arbre à partir d'un tableau d'enfants, du nombre d'enfants et du
     * poids de la racine.
     * Suppose que le tableau [enfants] est initialisé.
     */
    struct dag *d = malloc(sizeof(*d));

    d->enfants = enfants;
    d->n_enfants = n_enfants;
    d->poids = poids;
    d->refs = 0;

    for (int i = 0; i < n_enfants; i++) {
        (d->enfants[i])->refs += 1;
    }

    return d;
}


void supprime(struct dag *d)
{
    /* Libère l'arbre passé en argument, sauf si celui-ci est utilisé au sein
     * d'un autre DAG.
     */
    if (d != NULL) {
        for(int i = 0; i < d->n_enfants; i++) {
            (d->enfants[i])->refs -= 1;
            if ((d->enfants[i])->refs <= 0) {
                supprime(d->enfants[i]);
            }
        }

        free(d->enfants);
        free(d);
    }
}


int main(int argc, char **argv)
{
    struct dag *a, *b, **b_enfants;

    a = cree(NULL, 0, 1);
    b_enfants = malloc(sizeof(*b_enfants) * 2);
    b_enfants[0] = a;
    b_enfants[1] = a;

    b = cree(b_enfants, 2, 1);

    printf("%d\n", poids_total(b));
    supprime(b);

    return 0;
}
