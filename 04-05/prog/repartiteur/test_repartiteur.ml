open Repartiteur

(* best_fit *)

let () =
    let tache = (1, 1., "42") in
    assert (repartiteur tache [] 0 = (tache :: []) :: [])

let () =
    let tache1 = (1, 0.5, 42) in
    let serv1 = (2, 0.6, 88) :: [] in
    let serv2 = (1, 0.2, 11) :: (19, 0.2, 9) :: [] in
    let serv3 = (1, 0.3, 27) :: [] in
    let servs = (serv1 :: serv2 :: serv3 :: []) in
    let servs_new = best_fit tache1 servs 0 in
    let expected = serv1 :: (serv2 @ (tache1 :: [])) :: serv3 :: [] in
    assert (servs_new = expected);

    let tache2 = (1, 0.8, 32) in
    let servs_newer = best_fit tache2 servs_new 0 in
    let expected = (
        serv1
        :: (serv2 @ (tache1 :: []))
        :: serv3
        :: (tache2 :: [])
        :: []
    ) in
    assert (servs_newer = expected)

let () =
    let tache = (1, 0.1, ()) in
    let serv1 = (42, 0.5, ()) :: [] in
    let serv2 = (13, 0.25, ()) :: (29, 0.25, ()) :: [] in
    assert (
        best_fit tache (serv1 :: serv2 :: []) 0
        = (serv1 @ (tache :: [])) :: serv2 :: []
    );
    assert (
        best_fit tache (serv2 :: serv1 :: []) 0
        = (serv2 @ (tache :: [])) :: serv1 :: []
    )


(* lowest_overload *)

let get_duree_max (taches: 'a tache list): int =
    (* Renvoie la durée de la tâche la plus longue (0 si aucune tâches) *)
    List.fold_left (
        fun duree_max (duree, _, _) -> max duree duree_max
    ) 0 taches


let surcharge (servs: 'a etat_servers): float =
    (* Calcule la surcharge totale d'un ensemble de serveurs *)
    let duree_max = get_duree_max (List.flatten servs) in
    let rec aux (servs: 'a etat_servers) (accu: float): float =
        match servs with
        | [] -> accu
        | s :: servs ->
            let surch = energie s duree_max -. 1.0 in
            if surch > 0.0 then
                aux servs (accu +. surch)
            else
                aux servs accu
    in
    aux servs 0.0


let tous_les_choix (tache: 'a tache) (servs: 'a etat_servers)
    (max_servs: int): 'a etat_servers list
=
    (* Renvoie tous les états serveurs qu'un répartiteur pourrait
     * renvoyer (à permutation des tâches près)
     *)
    let limit = max_servs <> 0 in
    let rec aux (depth: int) (servs: 'a etat_servers): 'a etat_servers list =
        match servs with
        | [] ->
            if limit && depth >= max_servs then
                raise (Failure "tous_les_choix")
            else
                ((tache :: []) :: []) :: []
        | serv :: [] when limit && depth + 1 >= max_servs ->
            (* On ne peut pas créer de nouveaux serveurs *)
            ((tache :: serv) :: []) :: []
        | s :: servs ->
            let new_servs = aux (depth + 1) servs in
            let worlds_without: 'a etat_servers list =
                List.map (List.cons s) new_servs in
            let world_with: 'a etat_servers = (tache :: s) :: servs in
            world_with :: worlds_without
    in
    aux 0 servs


let surcharge_minimale (tache: 'a tache) (servs: 'a etat_servers)
    (max_servs: int): float
=
    (* Renvoie la surcharge minimale d'un ajout de la tache *)
    let tout = tous_les_choix tache servs max_servs in
    List.fold_left min infinity (List.map surcharge tout)


let () =
    let tache = (3, 0.5, 42) in
    let serv1 = (2, 0.6, 88) :: [] in
    let serv2 = (1, 0.9, 11) :: (19, 0.2, 9) :: [] in
    let serv3 = (1, 1.0, 27) :: [] in
    let servs = serv1 :: serv2 :: serv3 :: [] in
    let smin3 = surcharge_minimale tache servs 3 in
    assert (surcharge (lowest_overload tache servs 3) = smin3);
    let smin0 = surcharge_minimale tache servs 0 in
    assert (surcharge (lowest_overload tache servs 0) = smin0)

let () = print_endline "Tous les tests réussis avec succès"
