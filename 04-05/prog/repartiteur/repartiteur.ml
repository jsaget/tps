(** Gestion de la répartition de charges en OCaml *)

type 'a tache = int * float * 'a    (* durée, charge, data *)

type 'a server = 'a tache list

type 'a etat_servers = 'a server list

type 'a repartiteur = 'a tache -> 'a etat_servers -> int -> 'a etat_servers


(** Best-fit *)

let charge (serv: 'a server): float =
    (* Calcul la charge d'un serveur *)
    let rec aux (taches: 'a server) (f: float): float =
        match taches with
        | [] -> f
        | (_, cout, _) :: taches -> aux taches (f +. cout)
    in
    aux serv 0.


let best_fit: 'a repartiteur = fun tache servs _ ->
    let _, cout, _ = tache in
    let charge_max = 1. -. cout in
    let rec aux (servs: 'a etat_servers): float * 'a etat_servers =
        (* Le flottant indique la capaité initiale du serveur choisi pour
           accueillir la tâche, 0. si serveur créé. *)
        match servs with
        | [] -> (* Aucun serveur : on crée un nouveau serveur *)
            0., (tache :: []) :: []
        | serv :: servs ->
            (* Insérer récursivement, puis comparer à l'insertion dans ce
               serveur (si possible) *)
            let cost, new_servs = aux servs in
            let charge_serv = charge serv in
            if cost <= charge_serv && charge_serv <= charge_max then
                (* On préfère le serveur courant *)
                charge_serv, (serv @ (tache :: [])) :: servs
            else
                cost, serv :: new_servs
    in
    snd (aux servs)


(** Répartiteur *)

let energie (serv: 'a server) (temps: int): float =
    let rec aux (taches: 'a server) (f: float): float =
        match taches with
        | [] -> 0.
        | (duree, cout, _) :: taches ->
            let tps = float_of_int (min temps duree) in
            aux taches (f +. tps *. cout)
    in
    aux serv 0.


let lowest_overload: 'a repartiteur = fun tache servs max_servs ->
    (* max_servs <> 0 *)
    let duree, charge, _ = tache in
    let energie_max = 1. -. (charge *. (float_of_int duree)) in
    let rec aux (depth: int) (servs: 'a etat_servers): float *'a etat_servers
    =
        (* Le flottant indique l'énergie du serveur sur la durée pertinente. *)
        match servs with
        | [] ->
            if depth >= max_servs then
                raise (Failure "lowest_overload") (* Ne devrait pas arriver *)
            else
                0., (tache :: []) :: []
        | serv :: [] when depth + 1 >= max_servs ->
            (* On ne peut pas créer de nouveaux serveurs *)
            energie serv duree, (serv @ (tache :: [])) :: []
        | serv :: servs ->
            (* Si on peut choisir le serveur actuel, on ne cherche même pas
               récursivement *)
            let e_serv = energie serv duree in
            if e_serv <= energie_max then
                e_serv, (serv @ (tache :: [])) :: servs
            else
                let e_chosen, new_servs = aux (depth + 1) servs in
                if e_serv < e_chosen then
                    e_chosen, serv :: new_servs
                else
                    e_serv, (serv @ (tache :: [])) :: servs
    in
    snd (aux 0 servs)


let repartiteur: 'a repartiteur = fun tache servs max_servs ->
    if max_servs = 0 then
        best_fit tache servs max_servs
    else
        lowest_overload tache servs max_servs
