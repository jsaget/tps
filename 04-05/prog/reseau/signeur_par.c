#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

#define MAX_THREADS 100 // Indique le nombre maximal de threads pouvant tourner
                        // en parallèle
#define MAX_BUF_LEN 512 // Longueur maximale d'un message recu
#define BACKLOG 10  // Nombre max de connections en attente

// === VARIABLES GOLBALES pour la gestion des threads =========================
pthread_mutex_t mutex;
sem_t semaphore;
// === VARIABLES PARTAGÉES accesibles uniquement en section critique ==========
unsigned int *tampon, taille, next;
// ============================================================================

void sign()
{
    /* Signe les valeurs stockées dans le tampon et réinitialise la valeur de
     * next.
     * DOIT être appelé en section critique.
     */

    int i;

    for (i = 1; i < next; i++) {
        tampon[0] = tampon[0] ^ tampon[i];
    }

    next = 1;
}


void add(unsigned int x)
{
    /* Ajoute la valeur x au tableau tampon, en effectuant au préalabre une
     * signature si nécessaire.
     * DOIT être appelé en section critique
     */

    if (next == taille) {
        sign();
    }

    tampon[next] = x;
    next++;
}


void *execute(void *ordre)
{
    /* Exécute l'ordre reçu ("sign" ou "add X" avec X un entier).
     */
    int x;

    if (strlen(ordre) == 4 && strncmp(ordre, "sign", 4) == 0) {
        pthread_mutex_lock(&mutex);
        sign();
        pthread_mutex_unlock(&mutex);
    } else if (strlen(ordre) > 4 && strncmp(ordre, "add ", 4) == 0) {
        x = (unsigned int) atoi(ordre + 4);
        usleep(100000); // 100ms, simule un traitement long
        pthread_mutex_lock(&mutex);
        add(x);
        pthread_mutex_unlock(&mutex);
    } else {
        // Commande non reconnue
        fprintf(stderr, "execute : ordre non reconnu\n");
    }

    sem_post(&semaphore);
}


void mainloop(int sock_fd)
{
    /* Boucle principale exécutée par le serveur.
     * Reçoit un descripteur de fichier correspondant au serveur.
     * Effectue les ordres reçus sur le réseau.
     */

    int client_fd;
    char comm_buf[MAX_BUF_LEN], retval[MAX_BUF_LEN];
    ssize_t buf_len;
    struct sockaddr client_addr;
    socklen_t client_addr_len;
    pthread_t tid;

    for (;;) {
        client_fd = accept(sock_fd, &client_addr, &client_addr_len);

        buf_len = recv(client_fd, comm_buf, MAX_BUF_LEN, 0);
        
        if (comm_buf[buf_len] != '\0') {
            comm_buf[buf_len+1] = '\0';
        }

        // printf("%s : %d, %d\n", comm_buf, buf_len, strlen(comm_buf));

        if (strlen(comm_buf) == 4 && strncmp(comm_buf, "stop", 4) == 0) {
            for (int i = 0; i < MAX_THREADS; i++) {
                sem_wait(&semaphore);
            }
            // Tous les threads sont finis : plus de section critique

            sign();
            sprintf(retval, "%d", tampon[0]);
            send(client_fd, retval, strlen(retval), 0);
            close(client_fd);
            return;
        } else {
            // On fait exécuter l'ordre
            sem_wait(&semaphore);
            pthread_create(&tid, NULL, execute, comm_buf);
            pthread_detach(tid);
        }

        // Un ordre par connexion
        close(client_fd);
    }
}


void init_tampon(int T)
{
    /* Initialise les variables partagées liées au tampon. */

    taille = T;
    next = 1;
    tampon = malloc(taille * sizeof(*tampon));
    tampon[0] = 0;
}


int signeur(int T, char *port)
{
    /* Signeur : lance le serveur sur le port spécifié et initialise les
     * variables partagées.
     * Renvoie la valeur finale du tampon.
     * Suppose T > 1.
     */

    int sock_fd;
    struct addrinfo hints, *res, *p;

    // Initialisation des variables partagées

    if (sem_init(&semaphore, 0, MAX_THREADS)) {
        perror("mainloop : sem_init\n");
        exit(1);
    }

    if (pthread_mutex_init(&mutex, NULL)) {
        fprintf(stderr, "mainloop : pthread_mutex_init\n");
        exit(1);
    }

    init_tampon(T);
    
    // Lancement du serveur
    // 1. Récupération des informations pertinentes
    memset(&hints, 0, sizeof(hints));

    hints.ai_family = AF_UNSPEC;        // IPv4 ou IPv6
    hints.ai_socktype = SOCK_STREAM;    // TCP
    hints.ai_flags = AI_PASSIVE;        // La socket pourra être lié

    if (getaddrinfo(NULL, port, &hints, &res)) {
        perror("signeur : getaddrinfo");
        exit(1);
    }

    // 2. Création et association du socket
    for (p = res; p != NULL; p = p->ai_next) {
        // 2.1 Création de la socket
        if ((sock_fd = socket(p->ai_family, p->ai_socktype,
                        p->ai_protocol)) == -1) {
            perror("signeur : socket");
            continue;
        }

        // Association de la socket à l'adresse
        if (bind(sock_fd, p->ai_addr, p->ai_addrlen) == -1) {
            perror("signeur : bind");
            close(sock_fd);
            continue;
        }

        break;
    }

    if (p == NULL) {
        fprintf(stderr,
                "signeur : impossible de créer et d'associer la socket\n");
        exit(1);
    }

    freeaddrinfo(res);

    // 3. Écoute sur le socket
    if (listen(sock_fd, BACKLOG) == -1) {
        perror("signeur : listen");
        exit(1);
    }

    printf("Serveur lancé\n");

    // Boucle principale du serveur
    mainloop(sock_fd);

    // Fin du serveur
    close(sock_fd);
    free(tampon);
}


int main(int argc, char **argv)
{
    int T;

    if (argc != 3) {
        fprintf(stderr, "USAGE : %s TAILLE PORT\n", argv[0]);
        exit(1);
    }

    T = atoi(argv[1]);

    if (T <= 1) {
        fprintf(stderr, "PORT doit être >= 2\n");
        exit(1);
    }

    printf("Lancement du signeur\n");
    signeur(T, argv[2]);
    printf("Signeur terminé avec succès\n");

    exit(0);
}
