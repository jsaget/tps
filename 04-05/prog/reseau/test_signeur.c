#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#ifndef MAX_BUF_LEN
#define MAX_BUF_LEN 512
#endif

float random_unit()
{
    /* Renvoie un nombre flottant aléatoire entre 0 et 1 (inclus) */
    return (float) rand() / ((float) RAND_MAX);
}

double test_signeur(int n_commandes, float ratio, char *ip, char *port)
{
    /* Envoie des commandes au signeur.
     * Ratio vaut (nombre de add) / (nombre de sign), mais on préfère
     * travailler avec (nombre de add) / (nombre de commandes).
     */

    int serv_fd;
    struct addrinfo hints, *res, *p;
    char comm_buf[MAX_BUF_LEN];
    clock_t begin, end, total;
    float repartition = ratio / (1 + ratio);

    total = 0;

    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    if (getaddrinfo(ip, port, &hints, &res) == -1) {
        perror("test_signeur : getaddrinfo");
        exit(1);
    }

    if (res == NULL) {
        fprintf(stderr, "test_signeur : aucune adresse correspondante\n");
        exit(1);
    }

    for (int i = 0; i < n_commandes; i++) {
        // Recherche du socket
        for (p = res; p != NULL; p = p->ai_next) {
            if ((serv_fd = socket(p->ai_family, p->ai_socktype,
                            p->ai_protocol)) == -1) {
                perror("test_signeur : socket");
            }

            break;
        }

        if (p == NULL) {
            fprintf(stderr, "test_signeur : impossible de créer la socket\n");
            exit(1);
        }

        if (connect(serv_fd, p->ai_addr, p->ai_addrlen)) {
            perror("test_signeur : connect");
            exit(1);
        }

        /*
        if (i == n_commandes) {
            sprintf(comm_buf, "stop\0");
        } else
        */
        if (random_unit() < repartition) {
            sprintf(comm_buf, "add %d\0", rand() % 10);
        } else {
            sprintf(comm_buf, "sign\0");
        }

        // printf("%s\n", comm_buf);

        begin = clock ();
        if (send(serv_fd, comm_buf, strlen(comm_buf) + 1, 0) == -1) {
            perror("test_signeur : send");
            exit(1);
        }

        recv(serv_fd, comm_buf, MAX_BUF_LEN, 0);
        end = clock ();

        total += end - begin;

        close(serv_fd);
    }

    return ((double) total) / ((double) CLOCKS_PER_SEC);
}


int main(int argc, char **argv)
{
    int n_commandes;
    float ratio;
    double temps;

    if (argc != 5) {
        fprintf(stderr, "USAGE : %s NB_COMMANDES RATIO IP PORT\n", argv[0]);
        exit(1);
    }

    n_commandes = atoi(argv[1]);

    if (n_commandes <= 0) {
        fprintf(stderr, "NB_COMMANDES doit être un entier strictement positif\n");
        exit(1);
    }

    ratio = atof(argv[2]);

    if (ratio < 0) {
        fprintf(stderr, "RATIO doit être un flottant positif ou nul\n");
        exit(1);
    }

    printf("Lancement des tests\n");
    temps = test_signeur(n_commandes, ratio, argv[3], argv[4]);
    printf("Tests terminés\n");
    printf("Temps total d'exécution par le serveur : %f secondes\n", temps);
    printf("Temps moyen par commande : %f secondes\n", temps / n_commandes);

    exit(0);
}
