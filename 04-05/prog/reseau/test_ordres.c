/* Module de test pour le fichier "signeur.c" */

#include "signeur.c"
#include <assert.h>

void main() {
    int T = 10;

    init_tampon(10);
    assert(next == 1);
    assert(taille == T);
    assert(tampon[0] == 0);
    tampon[T-1] = 0;    // Pas de segfault

    sign();
    assert(tampon[0] == 0);

    add(0);
    assert(next == 2);
    sign();
    assert(tampon[0] == 0 ^ 0);
    assert(next == 1);

    add(1);
    sign();
    assert(tampon[0] == 0 ^ 0 ^ 1);

    for (int i = 1; i <= T; i++) {
        assert(next == i);
        add(i);
    }
    assert(next == 2);

    printf("Tous les tests résussis avec succès\n");
    exit(0);
}
