open Unix

exception TODO



(** CONSTANTES GLOBALES *)

let char_nul = Char.chr 0



(** VARIABLES PARTAGÉES ACCESSIBLES AUX THREADS CLIENT
 * Acessibles uniquement en SECTION CRITIQUE.
 *)

(* Table de hachage contenant les utilisateurs et le descripteur de fichiers
 * associé. *)
let users: (string, out_channel) Hashtbl.t = Hashtbl.create 8
let users_mutex = Mutex.create ()



(** UTILITAIRES *)

let addr_to_string = function
  | ADDR_INET (ia, port) ->
     Format.sprintf "%s:%d" (string_of_inet_addr ia) port
  | ADDR_UNIX a -> a


(* Renvoie une couple (head, foot) tel que :
 * - head ne contient pas le caractère sep
 * - head ^ (Char.escaped sep) ^ foot = str
 * Lève Invalid_argument si sep n'est pas dans str.
 *)
let split_fst (str: string) (sep: char): string * string =
  let index = try
      String.index str sep
    with Not_found -> raise (Invalid_argument "Séparateur non trouvé")
  in
  let head = String.sub str 0 index in
  let foot = String.sub str (index + 1) (String.length str - (index + 1)) in
  assert (String.equal str (head ^ (Char.escaped sep) ^ foot));
  (head, foot)



(** PROTOCOLE *)

type tag = CONNECT | TEXT
let tag_connect : string = "CONNECT"
let tag_text    : string = "TEXT"

let serialise_tag (tag: tag): string =
  match tag with
  | CONNECT -> tag_connect
  | TEXT    -> tag_text

let sep: char = ':'

type ordre = { tag: tag; payload: string }

let serialise_ordre (ordre: ordre): string =
  (serialise_tag ordre.tag) ^ (Char.escaped sep) ^ (ordre.payload)
  ^ "\n"

(* Exceptions *)
(* L'ordre n'est pas de la forme "CONNECT|TEXT:[^ \0\n]+" *)
exception Mauvais_format

(* L'ordre ne respecte pas le protocole (premier ordre autre que CONNECT, ou
 * ordre suivant CONNECT)
 *)
exception Mauvais_ordre

(* Le surnom est déjà utilisé *)
exception Nick_utilise

(* Le surnom n'est pas valide *)
exception Nick_invalide

(* Connexion interrompue *)
exception Connexion_interrompue


(* Récupération et traitement des ordres *)

(* Vérifie que la chaîne de caractère obéit aux contraites d'un payload *)
let is_valid_payload (payload: string): bool =
    String.for_all (fun c -> c <> char_nul && c <> '\n') payload

(* Parse l'ordre donné en argument et le renvoie.
 * Lève Mauvais_format si l'ordre n'a pas le format spécifié.
 *)
let parse_ordre (ordre: string): ordre =
  let tag_txt, payload = try
      split_fst ordre sep
    with Invalid_argument _ -> raise Mauvais_format
  in

  if not (is_valid_payload payload) then
    raise Mauvais_format;

  let tag =
    if String.equal tag_txt tag_connect then
      CONNECT
    else if String.equal tag_txt tag_text then
      TEXT
    else
      raise Mauvais_format
  in
  { tag; payload }


(* Renvoie vrai si le nick respecte les règles *)
let is_valid_nick (str: string): bool =
  let is_valid_nick_char (c: char): bool =
    let code = Char.code c in
    (code >= Char.code 'a' && code <= Char.code 'z')
    || (code >= Char.code 'A' && code <= Char.code 'Z')
    || Char.equal c '-'
    || Char.equal c '_'
    || Char.equal c '['
    || Char.equal c ']'
  in
  (String.length str <= 20)
  && (String.for_all is_valid_nick_char str)


(* Si le nick est présent dans la table de hachage, renvoie true
 * Sinon, mets le nick dans la table avec l'attribut spécifié et renvoie faux
 *)
let test_and_set_nick (nick: string) (outc: out_channel): bool =
  let test = Hashtbl.mem users nick in
  if not test then Hashtbl.add users nick outc;
  test


(* Envoie l'ordre à tous les clients sauf nick *)
let diffuse_ordre (nick: string) (ordre: string): unit =
  let ecrit_client (username: string) (outc: out_channel): unit =
    if username <> nick then begin
      output_string outc ordre;
      flush outc
    end
  in
  Mutex.lock users_mutex;
  Hashtbl.iter ecrit_client users;
  Mutex.unlock users_mutex

(** Gestion du client *)

let obtient_ordre (com: in_channel): ordre =
  parse_ordre (input_line com)


let handle_client (socket: Unix.file_descr) (inc: in_channel)
    (outc: out_channel) (s_addr: string): unit =
  let the_nick: string option ref = ref None in

  let ferme_connexion (): unit =
    try close_in  inc    with Sys_error _ -> ();
    try close_out outc   with Sys_error _ -> ();
    try close     socket with Sys_error _ -> ()
  in

  begin try
      (* 1. Connexion et vérification *)
      Format.printf "[%s] Connexion d'un nouveau client\n%!" s_addr;
      let fst_ordre = obtient_ordre inc in
      if fst_ordre.tag <> CONNECT then raise Mauvais_ordre;

      let nick = fst_ordre.payload in
      if not (is_valid_nick nick) then raise Nick_invalide;
      Mutex.lock users_mutex;
      if test_and_set_nick nick outc then raise Nick_utilise;
      output_string outc "CONNECT:OK\n";
      flush outc;
      Mutex.unlock users_mutex;
      the_nick := Some nick;
      
      Format.printf "[%s] %s a rejoint le salon\n%!" s_addr nick;

      (* 2. Boucle principale d'exécution *)
      while true do
        let ordre =
          try obtient_ordre inc with End_of_file -> raise Connexion_interrompue
        in
        Format.printf "[%s] Ordre reçu : %s%!" s_addr (serialise_ordre ordre);

        match ordre.tag with
        | TEXT ->
          let contenu = ordre.payload in
          let payload = nick ^ "> " ^ contenu ^ (Char.escaped '\n') in
          let ordre_envoi = serialise_ordre { tag = TEXT; payload } in
          diffuse_ordre nick ordre_envoi
        | _ -> raise Mauvais_ordre
      done

    with
    | Mauvais_format        -> Format.printf "[%s] Ordre invalide reçu\n%!"   s_addr
    | Mauvais_ordre         -> Format.printf "[%s] Ordre innatendu reçu\n%!"  s_addr
    | Nick_utilise          -> Format.printf "[%s] Nick déjà utilisé\n%!"     s_addr
    | Nick_invalide         -> Format.printf "[%s] Nick invalide\n%!"         s_addr
    | Connexion_interrompue -> Format.printf "[%s] Connexion interrompue\n%!" s_addr
  end;
  begin match !the_nick with
    | Some n ->
      Mutex.lock users_mutex;
      Hashtbl.remove users n;
      Mutex.unlock users_mutex;
      Format.printf "[%s] %s a quitté le salon\n%!" s_addr n
    | None -> ()
  end;
  ferme_connexion ();
  Format.printf "[%s] Connexion fermée\n%!" s_addr



(** Fonction principale *)

let main () =
  let server_socket = socket PF_INET SOCK_STREAM 0 in
  setsockopt server_socket SO_REUSEADDR true;
  bind server_socket (ADDR_INET (inet_addr_any, 9000));
  listen server_socket 100;
  Format.printf "Démarrage du serveur\n%!";
  while true do
    let cl_socket, addr = accept server_socket in
    let inc = in_channel_of_descr cl_socket in
    let outc = out_channel_of_descr cl_socket in
    let s_addr = addr_to_string addr in
    Format.printf "Connexion depuis %s\n%!" s_addr;
    ignore
      (Thread.create (handle_client cl_socket inc outc) s_addr)
  done

let () = main ()
