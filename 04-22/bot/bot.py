import socket
from time import time

def bot(serv, socket, dbcn):
    """Enregistre tout ce qui est envoyé par le serveur serv
    sur la socket dans la base de donnée dénotée par dbcn."""
    f = socket.makefile() #transforme la socket en fichier
    while True:
        line = f.readline()
        fields = line.split(':')
        if fields[0] != "TEXT":
            continue
        fields = fields[1].split('>')
        nick = fields[0]
        msg = fields[1]
        log_message(serv, nick, int(time()), msg, dbcn)
        if msg.startswith(" !STATS"):
            print_stats(serv, f, dbcn)

#Le reste du code établit une connexion à la base de données
#(dbcn) et au serveur de discussion (socket) puis appelle
#la fonction bot
