# cur = dcbn.cursor()
# tab = cur.execute("SELECT * FROM T')

tab = [
    { "name": "alice",      "n_msg": 2,     "fav_serv": "rezosup.fr", },
    { "name": "bob",        "n_msg": 3,     "fav_serv": "agreg-info.org", },
    { "name": "charlie",    "n_msg": 5,     "fav_serv": "rezosup.fr", },
    { "name": "david",      "n_msg": 8,     "fav_serv": "rezosup.fr", },
    { "name": "eve",        "n_msg": 13,    "fav_serv": "libera.chat", },
    { "name": "garance",    "n_msg": 21,    "fav_serv": "irc.gouv.fr", },
]


with open("./data.html", "wt") as f:
    f.write("<!DOCTYPE html5><html><head><title>Résultats</title></head><body><h1>Résultats</h1><table><tr>\n")
    for key in tab[0]:
        f.write("<th>")
        f.write(key)
        f.write("</th>")
    
    f.write("</tr>\n")

    for row in tab:
        f.write("<tr>")
        for key in row:
            f.write("<td>")
            f.write(str(row[key]))
            f.write("</td>")
        f.write("</tr>\n")
    
    f.write("</table></body></html>\n")
