#ifndef SCREEN_H
#define SCREEN_H
/* Type abstrait représentant l'écran.
   L'accès concurrent au même écran n'est pas sûr
*/
struct screen;

/* Initialise l'écran. Ce dernier est effacé,
  le séparateur est dessiné et la zone de saisie
  est initialisée avec le prompt donné */
struct screen *screen_init(const char * prompt);

/* Détruit et l'écran */
void screen_free(struct screen * sc);

/* La fonction bloque jusqu'à ce que l'utilisateur
   saisisse une chaine non vide et la valide avec
   la touche entrée. La chaine renvoyée doit être
   désalouée avec free.
*/
char *screen_read_input(struct screen *);

/* Affiche la chaine txt à la ligne line sur l'écran.
   Les numéros de ligne commencent à 1. Si le numéro de ligne
   est invalide, la fonction ne fait rien. Les caractères
   allant au delà de la dernière colonne sont ignorés.
*/
void screen_print_line(struct screen * sc, int line, char * txt);

/* Initialise les pointeurs donnés respectivement avec le nombre
   de lignes et de colonnes de l'écran.
*/
void screen_get_size(struct screen *sc, int *lines, int *cols);


#endif
