#include "screen.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>

#define MAX_BUF_LEN 512
#define MAX_NICK_LEN 20

struct history {
    char *msg;
    struct history *next;
};


/* VARIABLES PARTAGÉES */
struct screen *sc;
int lines, cols;
struct history *hist;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
/* */



void free_history(struct history *hist)
{
    struct history *next;

    if (hist == NULL) {
        return;
    }

    next = hist->next;
    free(hist);
    free_history(next);
}


void print_history()
{
    // Affiche l'historique sur l'écran
    struct history *node;
    int line_no;

    node = hist;

    for (line_no = lines-2; line_no > 0 && node != NULL; line_no--) {
        // fprintf(stderr, "[print_history] node_id: %p, msg_id: %p, msg: %s", node, node->msg, node->msg);
        screen_print_line(sc, line_no, node->msg);
        node = node->next;
    }
}


void ajoute_et_affiche(char *new_msg)
{
    // Ajoute le message msg à l'historique et affiche l'historique
    struct history *new_hist = malloc(sizeof(*new_hist));

    pthread_mutex_lock(&mutex);
    new_hist->msg = new_msg;
    new_hist->next = hist;
    hist = new_hist;
    print_history(hist);
    pthread_mutex_unlock(&mutex);
}


/* Thread qui gère l'affichage */

void afficheur(int serv_fd)
{
    char recv_buf[MAX_BUF_LEN];
    int recv_buf_len;

    char valid_start[] = "TEXT:";
    int valid_start_len = strlen(valid_start);

    hist = NULL;

    for (;;) {
        if ((recv_buf_len = recv(serv_fd, recv_buf, MAX_BUF_LEN, 0)) == -1) {
            perror("afficheur: recv");
            continue;
        }

        recv_buf[recv_buf_len] = '\0';

        if (strncmp(recv_buf, valid_start, valid_start_len)) {
            fprintf(stderr, "Le message envoyé pas le serveur ne respecte pas le protocole :\n");
            fprintf(stderr, recv_buf);
            continue;
        }

        ajoute_et_affiche(recv_buf + valid_start_len);
    }
}

void *afficheur_thread(void *serv_fd) {
    afficheur(*(int *)serv_fd);
    return NULL;
}


/* Thread qui gère la saisie */

void invite(int serv_fd, char *prompt)
{
    /* Attend que le client saisisse son message, puis l'envoie au serveur et
     * l'affiche.
     * Si le message commence par "/QUIT", déconnecte l'utilisateur.
     */
    
    char tag[] = "TEXT:";
    char quit[] = "/QUIT";
    char *data, *shown, ordre[MAX_BUF_LEN];
    
    for (;;) {
        data = screen_read_input(sc);

        if (strncmp(data, quit, strlen(quit)) == 0) {
            fprintf(stderr, "Ordre de quitter reçu");
            break;
        }

        shown = malloc((strlen(data) + strlen(prompt) + 1) * sizeof(*shown));
        sprintf(shown, "%s%s\n", prompt, data);

        snprintf(ordre, MAX_BUF_LEN, "%s:%s", tag, shown);

        if (send(serv_fd, ordre, strlen(ordre), 0) == -1) {
            perror("client: invite");
            exit(2);
        }

        ajoute_et_affiche(shown);

        free(data);
    }

    return;
}


struct invite_arg {
    int serv_fd;
    char *prompt;
};


void *invite_thread(void *ia_arg) {
    struct invite_arg *ia = (struct invite_arg *)ia_arg;
    invite(ia->serv_fd, ia->prompt);
    return NULL;
}



/* Initialisation de la connexion */


int check_nick(char *nick)
{
    /* Renvoie 0 si le nick est valide, -1 sinon. */
    char c;
    int len = strlen(nick);

    if (len == 0 || len > MAX_NICK_LEN) {
        return -1;
    }

    for (int i = 0; i < len; i++) {
        c = nick[i];
        if (isalnum(c) || c == '-' || c == '_' || c == '[' || c == ']') {
            continue;
        }
        return -1;
    }

    return 0;
}


/* Fonction principale */


void client(char *hote, char *service, char *nick)
{
    /* Lance la connexion au serveur et interagit avec lui. */
    struct addrinfo hints, *res, *p;
    int serv_fd;
    char com_buf[MAX_BUF_LEN];
    int com_buf_len;
    char connect_ok[] = "CONNECT:OK\n";
    int connect_ok_len = strlen(connect_ok);

    pthread_t tid_afficheur, tid_invite;
    struct invite_arg *ia = malloc(sizeof(*ia));;

    char prompt[MAX_NICK_LEN+3];

    // 1. Préparation des informations
    fprintf(stderr, "Récupération des informations de connexion\n");
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    getaddrinfo(hote, service, &hints, &res);

    // 2. Connexion au serveur
    fprintf(stderr, "Connexion au serveur\n");
    for (p = res; p != NULL; p = p->ai_next) {
        // 2.1 Création de la socket
        if ((serv_fd = socket(p->ai_family, p->ai_socktype,
                        p->ai_protocol)) == -1) {
            perror("socket");
            continue;
        }

        // 2.2 Établissement de la connexion
        if (connect(serv_fd, p->ai_addr, p->ai_addrlen) == -1) {
            perror("connect");
            close(serv_fd);
            continue;
        }

        fprintf(stderr, "Connexion établie\n");
        break;
    }

    if (p == NULL) {
        fprintf(stderr, "Impossible de se connecter au serveur\n");
        exit(2);
    }

    // 3. Test du protocole
    sprintf(com_buf, "CONNECT:%s\n", nick);
    send(serv_fd, com_buf, strlen(com_buf), 0);
    com_buf_len = recv(serv_fd, com_buf, MAX_BUF_LEN, 0);
    if (com_buf_len != connect_ok_len
            || strncmp(com_buf, connect_ok, connect_ok_len)) {
        fprintf(stderr, "Le serveur devait renvoyer\n");
        fprintf(stderr, connect_ok);
        fprintf(stderr, "Mais il a renvoyé");
        fprintf(stderr, "%s\n", com_buf);
        exit(2);
    }

    fprintf(stderr, "Le serveur a bien renvoyé %s", connect_ok);

    // 4. Mise en place de toute la logistique client
    
    snprintf(prompt, MAX_NICK_LEN+3, "%s> ", nick);
    sc = screen_init(prompt);
    screen_get_size(sc, &lines, &cols);
    fprintf(stderr, "Écran initialisé\n");

    ia->serv_fd = serv_fd;
    ia->prompt = prompt;

    hist = NULL;

    // 5. Lancement des threads
    fprintf(stderr, "Lancement des threads\n");

    if (pthread_create(&tid_afficheur, NULL, &afficheur_thread,
                &serv_fd) == -1) {
        perror("client: pthread_create");
        exit(2);
    }

    if (pthread_create(&tid_invite, NULL, &invite_thread, ia) == -1) {
        perror("client: pthread_create");
        exit(2);
    }

    fprintf(stderr, "Threads lancés, attente d'une demande de fin\n");
    pthread_join(tid_invite, NULL);

    // 6. Fin du serveur : nettoyage
    fprintf(stderr, "Déconnexion demandée\n");

    pthread_kill(tid_afficheur, SIGKILL);

    pthread_mutex_destroy(&mutex);
    screen_free(sc);
    free_history(hist);
    free(ia);

    fprintf(stderr, "Fermeture de la connexion\n");
    close(serv_fd);

    return;
}


int main(int argc, char **argv)
{
    char *nick;

    if (argc != 4) {
        fprintf(stderr, "Utilisation : %s SERVEUR PORT NICK\n", argv[0]);
        exit(1);
    }

    nick = argv[3];

    if (check_nick(nick)) {
        fprintf(stderr, "%s ne respecte pas les règles d'un nick valide", nick);
    }

    client(argv[1], argv[2], nick);

    return 0;
}
